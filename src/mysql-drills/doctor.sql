CREATE DATABASE patient_management;

USE patient_management;

CREATE TABLE secretary (id INT AUTO_INCREMENT, secretary_name VARCHAR(100), 
	PRIMARY KEY (id));
    
CREATE TABLE drugs(id INT AUTO_INCREMENT, drug VARCHAR(100), 
	PRIMARY KEY (id));
    
CREATE TABLE patient (id INT AUTO_INCREMENT, patient_name VARCHAR(100), dob DATETIME, 
    patient_address VARCHAR(100), 
    PRIMARY KEY (id));

CREATE TABLE doctor (id INT AUTO_INCREMENT, doctor_name VARCHAR(100), secretary_id INT, patient_id INT, 
	PRIMARY KEY (id), 
    FOREIGN KEY (secretary_id) REFERENCES secretary(id),
    foreign key (patient_id) REFERENCES patient(id));

CREATE TABLE prescription (id INT AUTO_INCREMENT, prescription_date DATETIME, patient_id INT, doctor_id INT, 
	PRIMARY KEY (id), 
    FOREIGN KEY (patient_id) REFERENCES patient(id), 
    FOREIGN KEY (doctor_id) REFERENCES doctor(id));
    
CREATE TABLE presc_drugs (id INT AUTO_INCREMENT, prescription_id INT, drug_id INT, dosage VARCHAR(100), 
	PRIMARY KEY (id), 
    FOREIGN KEY (prescription_id) REFERENCES prescription(id), 
    FOREIGN KEY (drug_id) REFERENCES drugs(id));