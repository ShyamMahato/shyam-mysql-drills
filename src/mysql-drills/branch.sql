CREATE DATABASE library_management;

USE library_management;

CREATE TABLE branch (id INT AUTO_INCREMENT, branch_address VARCHAR(100), 
	PRIMARY KEY(id));
    
CREATE TABLE author (id INT AUTO_INCREMENT, author_name VARCHAR(100), 
	PRIMARY KEY(id)); 
    
CREATE TABLE publisher (id INT AUTO_INCREMENT, publisher_name VARCHAR(100), 
	PRIMARY KEY(id));

CREATE TABLE book (id INT AUTO_INCREMENT, ISBN INT UNIQUE, title VARCHAR (100), publisher_id INT, 
	PRIMARY KEY(id),
	FOREIGN KEY (publisher_id) REFERENCES publisher(id));

CREATE TABLE book_author (id INT AUTO_INCREMENT, book_id INT, author_id INT, 
	PRIMARY KEY (id),
	FOREIGN KEY (book_id) REFERENCES book(id),
	FOREIGN KEY (author_id) REFERENCES author(id));

CREATE TABLE book_copies (id INT AUTO_INCREMENT, branch_id INT, book_id INT, num_copies INT,
	PRIMARY KEY (id),
	FOREIGN KEY (branch_id) REFERENCES branch(id),
    FOREIGN KEY (book_id) REFERENCES book(id));

