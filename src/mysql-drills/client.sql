CREATE DATABASE contract_management;

USE contract_management;

CREATE TABLE location_tbl (id INT AUTO_INCREMENT, location_name VARCHAR(100), 
	PRIMARY KEY (id));

CREATE TABLE client_tbl (id INT AUTO_INCREMENT, client_name VARCHAR(100), location_id INT,
	PRIMARY KEY (id),
    FOREIGN KEY (location_id) REFERENCES location_tbl(id));

CREATE TABLE manager (id INT AUTO_INCREMENT, manager_name VARCHAR(100), location_id INT,
	PRIMARY KEY (id),
    FOREIGN KEY (location_id) REFERENCES location_tbl(id));

CREATE TABLE contract_tbl (id INT AUTO_INCREMENT, client_id INT, estimated_cost INT, 
	completion_date datetime,
    PRIMARY KEY (id),
    FOREIGN KEY (client_id) REFERENCES client_tbl(id));
    
CREATE TABLE contract_manager(id INT AUTO_INCREMENT, contract_id INT, manager_id INT, 
	PRIMARY KEY (id),
    FOREIGN KEY (manager_id) REFERENCES manager(id),
    FOREIGN KEY (contract_id) REFERENCES contract_tbl(id));

CREATE TABLE staff (id INT AUTO_INCREMENT, staff_name VARCHAR(100), location_id INT,
	PRIMARY KEY (id),
    FOREIGN KEY (location_id) REFERENCES location_tbl(id)); 

CREATE TABLE contract_staff (id INT AUTO_INCREMENT, contract_id INT, staff_id INT, 
	PRIMARY KEY (id),
    FOREIGN KEY (contract_id) REFERENCES contract_tbl(id), 
    FOREIGN KEY (staff_id) REFERENCES staff(id));


